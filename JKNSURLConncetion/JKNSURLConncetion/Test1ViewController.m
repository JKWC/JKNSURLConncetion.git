//
//  Test1ViewController.m
//  JKNSURLConncetion
//
//  Created by 王冲 on 2019/1/21.
//  Copyright © 2019年 JK科技有限公司. All rights reserved.
//

#import "Test1ViewController.h"

@interface Test1ViewController ()

@end

@implementation Test1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor purpleColor];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    // http://images.ciotimes.com/2ittt-zm.mp4
    
    // 1、对视频链接进行编码
    // 在iOS程序中，访问一些HTTP/HTTPS的资源服务时，如果url中存在中文或者特殊字符时，会导致无法正常的访问到资源或服务，想要解决这个问题，需要对url进行编码。
    NSString *urlStr = @"http://images.ciotimes.com/2ittt-zm.mp4";
    
    // 在 iOS9 之后废弃了
    // urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    // iOS9 之后取代上面的 api
    urlStr = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    // 2、string 转 NSURL
    NSURL *url = [NSURL URLWithString:urlStr];
    
    // 3、创建 NSURLRequest 对象
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    // 4、NSURLConnection 下载 视频
    // iOS9 之后废弃了
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse * _Nullable response,NSData * _Nullable data, NSError * _Nullable connectionError) {
        
           // 下载视频的名字
           NSString *videoName =  [urlStr lastPathComponent];
           // 下载到桌面的文件夹 JK视频下载器
           NSString *downPath = [NSString stringWithFormat:@"/Users/wangchong/Desktop/JK视频下载器/%@",videoName];
           // 将数据写入到硬盘
           [data writeToFile:downPath atomically:YES];
                               
           NSLog(@"下载完成");
        
    }];
    
    
    
}


@end
