//
//  ViewController.m
//  JKNSURLConncetion
//
//  Created by 王冲 on 2019/1/21.
//  Copyright © 2019年 JK科技有限公司. All rights reserved.
//

#import "ViewController.h"
// 1、NSURLConnection简单的使用
#import "Test1ViewController.h"
// 2、NSURLConnection 进度监听
#import "Test2ViewController.h"
// 3、拼接数据然后写入磁盘
#import "Test3ViewController.h"
// 4、NSFileHandle数据包边下载边写入磁盘
#import "Test4ViewController.h"
// 5、NSOutputStream拼接文件
#import "Test5ViewController.h"
// 6、解决NSURLConncetion的下载在主线程的问题
#import "Test6ViewController.h"

@interface ViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView *tableView;

@property(nonatomic,strong) NSMutableArray *dataArray;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"下载器";
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    [self.dataArray addObjectsFromArray:@[@"NSURLConnection简单的使用",@"NSURLConnection 进度监听",@"拼接数据然后写入磁盘",@"NSFileHandle数据包边下载边写入磁盘",@"NSOutputStream拼接文件",@"解决NSURLConncetion的下载在主线程的问题"]];
    
    [self.view addSubview:self.tableView];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    
    if (!cell) {
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellID"];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"%ld、 %@",indexPath.row,self.dataArray[indexPath.row]];
    cell.backgroundColor = JKRandomColor;
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80;
}

-(UITableView *)tableView{
    
    if (!_tableView) {
        
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 44+JKstatusBarHeight, JK_SCREEN_WIDTH, JK_SCREEN_HEIGHT-44-JKstatusBarHeight) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        if (@available(iOS 11.0, *)) {
            
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            
        } else {
            // 小于11.0的不做操作
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
    }
    
    return _tableView;
}

-(NSMutableArray *)dataArray{
    
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    
    return _dataArray;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cell_name = [NSString stringWithFormat:@"%@",self.dataArray[indexPath.row]];
    if ([cell_name isEqualToString:@"NSURLConnection简单的使用"]) {
        
        Test1ViewController *test1ViewController = [Test1ViewController new];
        [self.navigationController pushViewController:test1ViewController animated:YES];
        
    }else if ([cell_name isEqualToString:@"NSURLConnection 进度监听"]){
        Test2ViewController *test2ViewController = [Test2ViewController new];
        [self.navigationController pushViewController:test2ViewController animated:YES];
    }else if ([cell_name isEqualToString:@"拼接数据然后写入磁盘"]){
        
        Test3ViewController *test3ViewController = [Test3ViewController new];
        [self.navigationController pushViewController:test3ViewController animated:YES];
    }else if ([cell_name isEqualToString:@"NSFileHandle数据包边下载边写入磁盘"]){
        
        Test4ViewController *test4ViewController = [Test4ViewController new];
        [self.navigationController pushViewController:test4ViewController animated:YES];
    }else if ([cell_name isEqualToString:@"NSOutputStream拼接文件"]){
        
        Test5ViewController *test5ViewController = [Test5ViewController new];
        [self.navigationController pushViewController:test5ViewController animated:YES];
    }else if ([cell_name isEqualToString:@"解决NSURLConncetion的下载在主线程的问题"]){
        
        Test6ViewController *test6ViewController = [Test6ViewController new];
        [self.navigationController pushViewController:test6ViewController animated:YES];
    }
}

@end
