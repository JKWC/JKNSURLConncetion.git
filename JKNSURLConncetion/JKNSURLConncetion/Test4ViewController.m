//
//  Test4ViewController.m
//  JKNSURLConncetion
//
//  Created by 王冲 on 2019/1/21.
//  Copyright © 2019年 JK科技有限公司. All rights reserved.
//

#import "Test4ViewController.h"

/**
 NSURLConnectionDownloadDelegate ： 不要使用，它是专门针对杂志下载提供的接口
 如果使用它下载，可以监听到下载进度，但是找不到下载的文件
 Newsstand Kit’s downloadWithDelegate: 专门用来做杂志的Kit
 */
@interface Test4ViewController ()<NSURLConnectionDataDelegate>

@property(nonatomic,strong) UILabel *progressLabel;

/**
 要下载的文件总大小
 */
@property(nonatomic,assign) long long exceptedContentLength;

/**
 当前已经下载的文件总大小
 */
@property(nonatomic,assign) long long currentDownContentLength;

/**
 保存下载视频的路径
 */
@property(nonatomic,strong) NSString *downFilePath;

@end

@implementation Test4ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor purpleColor];
    
    [self.view addSubview: self.progressLabel];
}

-(UILabel *)progressLabel{
    
    if (!_progressLabel) {
        
        _progressLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 100, JK_SCREEN_WIDTH-40, 100)];
        _progressLabel.backgroundColor = [UIColor yellowColor];
        _progressLabel.textColor = [UIColor brownColor];
    }
    
    return _progressLabel;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    // http://images.ciotimes.com/2ittt-zm.mp4
    
    // 1、对视频链接进行编码
    // 在iOS程序中，访问一些HTTP/HTTPS的资源服务时，如果url中存在中文或者特殊字符时，会导致无法正常的访问到资源或服务，想要解决这个问题，需要对url进行编码。
    NSString *urlStr = @"http://images.ciotimes.com/2ittt-zm.mp4";
    
    // 在 iOS9 之后废弃了
    // urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    // iOS9 之后取代上面的 api
    urlStr = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    // 2、string 转 NSURL
    NSURL *url = [NSURL URLWithString:urlStr];
    
    // 3、创建 NSURLRequest 对象
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    // 4、NSURLConnection 下载 视频
    NSURLConnection *connection = [[NSURLConnection alloc]initWithRequest:request delegate:self];
    [connection start];
    
    
}

/**
 1、即将发送Request
 - (nullable NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(nullable NSURLResponse *)response;
 2、接收服务器的响应
 - (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response;
 3、接收服务器的数据
 - (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data;
 4、需要文件流
 - (nullable NSInputStream *)connection:(NSURLConnection *)connection needNewBodyStream:(NSURLRequest *)request;
 - (void)connection:(NSURLConnection *)connection   didSendBodyData:(NSInteger)bytesWritten
 totalBytesWritten:(NSInteger)totalBytesWritten
 totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite;
 
 - (nullable NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse;
 接收到所有的数据加载完毕
 - (void)connectionDidFinishLoading:(NSURLConnection *)connection;
 */

// 1、接收服务器的响应 --- 状态和响应头做一些准备工作
// expectedContentLength : 文件的总大小
// suggestedFilename : 服务器建议保存的名字
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    JKLog(@"\nURL=%@\nMIMEType=%@\ntextEncodingName=%@\nsuggestedFilename=%@",response.URL,response.MIMEType,response.textEncodingName,response.suggestedFilename);
    
    // 记录文件的总大小
    self.exceptedContentLength = response.expectedContentLength;
    // 当前下载的文件大小初始化为 0
    self.currentDownContentLength = 0;
    
    // 创建下载的路径
    self.downFilePath = [@"/Users/wangchong/Desktop/JK视频下载器" stringByAppendingPathComponent:response.suggestedFilename];
    
    //删除 如果路径不存在时，就什么的不做！ 就文件存在是，就会直接删除
    [[NSFileManager defaultManager]removeItemAtPath:self.downFilePath error:nil];
    
}

// 2、接收服务器的数据，由于数据是分块发送的，这个代理方法会被执行多次，因此我们也会拿到多个data
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    // JKLog(@"接收到的数据长度=%tu",data.length);
    self.currentDownContentLength += data.length;
    
    // 计算百分比
    // progress = (float)long long / long long
    float progress = (float)self.currentDownContentLength/self.exceptedContentLength;
    
    JKLog(@"下载的进度=%f",progress);
    
    self.progressLabel.text = [NSString stringWithFormat:@"下载进度：%f",progress];
    
    [self writeFileData:data];
 
    
}

// 3、接收到所有的数据加载完毕后会有一个通知
- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    JKLog(@"下载完毕");
    
}

// 把数据写入到磁盘的方法
-(void)writeFileData:(NSData *)data{

    /**
      NSFileManager：主要的功能是创建目录、检查目录是否存在、遍历目录、删除文件
                    拷贝文件、剪切文件等等，主要是针对文件的操作
      NSFileHandle：文件"句柄"，z对文件的操作，主要功能是：对同一个文件进行二进制读写
     
      判断文件是否下载完成 ？
      答：判断进度？判断完成通知？，判断时间？判断大小？这些都不太好，比较好的方式是使用MD5
      MD5: 1.服务器对你下载的文件计算好一个MD5，将此 MD5 传给客户端
           2.开始下载文件......
           3.下载完成时，对下载的文件做一次MD5
           4.比较服务器返回的MD5和我们自己计算的MD5，如果二者相等，就代表下载完成
     
     */
    
    NSFileHandle *fp = [NSFileHandle fileHandleForWritingAtPath:self.downFilePath];
    
    // 如果文件不存在，直接x将数据写入磁盘
    if (fp == nil) {
        
        [data writeToFile:self.downFilePath atomically:YES];
    }else{
        
        // 如果存在，将data追加到现在文件的末尾
        [fp seekToEndOfFile];
        // 写入文件
        [fp writeData:data];
        // 关闭文件
        [fp closeFile];
    }
    
    
}

// 4、下载错误的处理
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    JKLog(@"链接失败");
    
}




@end
